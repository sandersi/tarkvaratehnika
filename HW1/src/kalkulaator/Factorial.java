package kalkulaator;

public class Factorial {

	public static int factorial(int x) {
		int res = 1;
		for (int i = 1; i <= x; i++) {
			res *= i;
		}
		return res;
	}
	
}
