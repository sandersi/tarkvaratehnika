package kalkulaator;

public class Sum {

	public static int add(String numbers) {
		String[] nums = numbers.split(",|;");
		int res = 0;
		/*if(nums.length > 2) {
			throw new RuntimeException("Up to 2 numbers separated by comma (,) are allowed");
		} else if (numbers.isEmpty()){
			return 0;
		} else {*/
		for (String number : nums) {
			if(numbers.isEmpty()) return 0;
			if(Integer.parseInt(number) < 0 ) throw new RuntimeException("Negatives not allowed");
			res += Integer.parseInt(number);
		}
		return res;
		
	}

	public static int addEven(int[] numbers) {
		int res = 0;
		for(int number : numbers) {
			if(number > 0 && number % 2 == 0) res += number;
		}
		return res;
	}

}
