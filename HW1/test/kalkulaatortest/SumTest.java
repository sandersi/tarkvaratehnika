package kalkulaatortest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import kalkulaator.Factorial;
import kalkulaator.Sum;

public class SumTest {
	
	/*@Test(expected = RuntimeException.class)
	public void testAddThreeNumbers() {
		Sum.add("1,2,3");
	}*/
	
	@Test
	public void testAddTwoNumbers() {
		assertEquals("Result is different than expected", 3, Sum.add("1,2"));
	}
	
	@Test(expected = RuntimeException.class)
	public void testAddNotNumbers() {
		assertEquals("Result is different than expected", 3, Sum.add("1,X"));
	}
	
	@Test
	public void testAddOneNumbers() {
		assertEquals("Result is different than expected", 5, Sum.add("5"));
	}
	
	@Test
	public void testAddNoNumbers() {
		assertEquals("Result is different than expected", 0, Sum.add(""));
	}
	
	@Test
	public void testAddAnyNumbers() {
		assertEquals("Result is different than expected", 3+6+15+21+5, Sum.add("3,6,15,21,5"));
	}
	
	@Test
	public void testPunktKomaSplit() {
		assertEquals("Result is different than expected", 3+6+15+21+5, Sum.add("3,6;15;21;5"));
	}
	
	@Test(expected = RuntimeException.class)
	public void testAddNegative() {
		Sum.add("3,-4");
	}
	
	@Test
	public void testAddOneEvenNumber() {
		assertEquals("Result is different than expected",2, Sum.addEven(new int[] {2}));
	}
	
	@Test
	public void testAddEvenNumbers() {
		assertEquals("Result is different than expected",2+4+6, Sum.addEven(new int[] {2,4,6}));
	}
	
	@Test
	public void testEvenIgnoreNegativeNumbers() {
		assertEquals("Result is different than expected",2+4+6, Sum.addEven(new int[] {2,-2,4,6,-10}));
	}
	
	@Test
	public void testEvenIgnoreOddNumbers() {
		assertEquals("Result is different than expected",2+4+6, Sum.addEven(new int[] {2,1,4,6,3}));
	}
	
	@Test
	public void testEvenIgnoreOddAndNegativeNumbers() {
		assertEquals("Result is different than expected",2+4+6, Sum.addEven(new int[] {2,1,4,6,3,-6}));
	}

}
