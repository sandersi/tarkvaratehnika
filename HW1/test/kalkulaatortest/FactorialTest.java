package kalkulaatortest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import kalkulaator.Factorial;

public class FactorialTest {
	
	@Test
	public void testOne() {
		assertEquals("Result is different then expected", 1, Factorial.factorial(1));
	}
	
	@Test
	public void testTwo() {
		assertEquals("Result is different then expected", 2, Factorial.factorial(2));
	}
	
	@Test
	public void testThree() {
		assertEquals("Result is different then expected", 6, Factorial.factorial(3));
	}
}
